<div class="card">
    <div class=card-header>
    <h3>How to get Token</h3>
    </div>
    <div class="card-body">
        <ul class="list-group">
            <li class="list-group-item">1. Register at <a href="https://wafvel.com/register" target="_blank" rel="noopener noreferrer">wafvel.com/register</a> - it's Free!</li>
            <li class="list-group-item">2. Add your whatsapp number on <a href="https://wafvel.com/whatsapp/create" target="_blank" rel="noopener noreferrer">add whatsapp page</a></li>
            <li class="list-group-item">3. Copy your token from <a href="https://wafvel.com/whatsapp" target="_blank" rel="noopener noreferrer">here</a></li>
            <li class="list-group-item">4. Scan QR (like you use whatsapp web) using your phone whatsapp <a href="https://wafvel.com/whatsapp" target="_blank" rel="noopener noreferrer">here</a></li>
        </ul>
    </div>
</div>